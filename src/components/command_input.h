#ifndef WASM_BOILERPLATE_COMPONENTS_H
#define WASM_BOILERPLATE_COMPONENTS_H

#include <emscripten.h>
#include <emscripten/val.h>
#include <iostream>
#include "asm-dom.hpp"

#include "../mup_parser/mpParser.h"
#include "../state/state.h"

extern void render();

extern mup::ParserX math_parser;

namespace COMPONENTS {

  /**
   * Input `onkeyup` handler
   * @param event
   * @return
   */
  bool handle_command_input_key_up (emscripten::val event);

  /**
   * Get the DOM for the command input
   * @return
   */
  asmdom::VNode* get_command_input();

}

#endif //WASM_BOILERPLATE_COMPONENTS_H