#ifndef WASM_BOILERPLATE_CLEAR_BUTTON_H
#define WASM_BOILERPLATE_CLEAR_BUTTON_H

#include <emscripten.h>
#include <emscripten/val.h>
#include <iostream>
#include "asm-dom.hpp"

#include "../mup_parser/mpParser.h"
#include "../state/state.h"

extern void render();

extern mup::ParserX math_parser;

namespace COMPONENTS {

  /**
   * Clear button `click` handler
   * @param event
   * @return
   */
  bool handle_clear_button_click (emscripten::val event);

  asmdom::VNode* get_clear_button ();

}

#endif //WASM_BOILERPLATE_CLEAR_BUTTON_H
